import pandas as pd
import json
import datetime
with open('data-active24h.json') as f:
  data = json.load(f)
elastic_docs = data['aggregations']['3']['buckets']
kq=[]
for num, doc in enumerate(elastic_docs):
    cmp     = {'time_per_minute': doc['key_as_string'],
            'filter':'*',
            'number_of_active_user':doc['4']['buckets']['*']['2']['value']}
    cmp1134 = {'time_per_minute': doc['key_as_string'],
            'filter':'company_id:1134',
            'number_of_active_user':doc['4']['buckets']['company_id:1134']['2']['value']}
    cmp1213 = {'time_per_minute': doc['key_as_string'],
            'filter':'company_id:1213',
            'number_of_active_user':doc['4']['buckets']['company_id:1134']['2']['value']}
    cmp636 = {'time_per_minute': doc['key_as_string'],
            'filter':'company_id:636',
            'number_of_active_user':doc['4']['buckets']['company_id:636']['2']['value']}
    kq.append(cmp)
    kq.append(cmp1213)
    kq.append(cmp636)
    kq.append(cmp1134)
#write to csv file
df= pd.DataFrame(kq)
df.to_csv('%s-Active Users.csv' % datetime.datetime.today().strftime('%Y-%m-%d'),index=None)
