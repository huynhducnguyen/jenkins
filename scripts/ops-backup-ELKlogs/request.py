import pandas as pd
import json
import datetime
with open('data-request24.json') as f:
  data = json.load(f)
elastic_docs = data['aggregations']['3']['buckets']
kq=[]
for num, doc in enumerate(elastic_docs):
    cmp     = {'time_per_minute': doc['key_as_string'],
            'filter':'*',
            'Number_of_Request':doc['4']['buckets']['*']['doc_count']}
    cmp1134 = {'time_per_minute': doc['key_as_string'],
            'filter':'company_id:1134',
            'Number_of_Request':doc['4']['buckets']['company_id:1134']['doc_count']}
    cmp1213 = {'time_per_minute': doc['key_as_string'],
            'filter':'company_id:1213',
            'Number_of_Request':doc['4']['buckets']['company_id:1134']['doc_count']}
    cmp636 = {'time_per_minute': doc['key_as_string'],
            'filter':'company_id:636',
            'Number_of_Request':doc['4']['buckets']['company_id:636']['doc_count']}
    kq.append(cmp)
    kq.append(cmp1213)
    kq.append(cmp636)
    kq.append(cmp1134)
#write to csv
df= pd.DataFrame(kq)
df.to_csv('%s-Requests.csv' % datetime.datetime.today().strftime('%Y-%m-%d'),index=None)
