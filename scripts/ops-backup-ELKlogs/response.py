import pandas as pd
import json
import datetime
with open('data-respons24.json') as f:
  data = json.load(f)
elastic_docs = data['aggregations']['3']['buckets']
kq=[]
for num, doc in enumerate(elastic_docs):
    cmp     = {'time_per_minute': doc['key_as_string'],
            'filter':'*',
            'Response_time':doc['4']['buckets']['*']['3']['values'][0]['value']}
    cmp1134 = {'time_per_minute': doc['key_as_string'],
            'filter':'company_id:1134',
            'Response_time': doc['4']['buckets']['company_id:1134']['3']['values'][0]['value']}
    cmp1213 = {'time_per_minute': doc['key_as_string'],
            'filter':'company_id:1213',
            'Response_time':doc['4']['buckets']['company_id:1134']['3']['values'][0]['value']}
    cmp636 = {'time_per_minute': doc['key_as_string'],
            'filter':'company_id:636',
            'Response_time':doc['4']['buckets']['company_id:636']['3']['values'][0]['value']}
    kq.append(cmp)
    kq.append(cmp1213)
    kq.append(cmp636)
    kq.append(cmp1134)
#print (kq)


  

#print (kq)

#write to csv
df= pd.DataFrame(kq)
df.to_csv('%s-Rails response time.csv' % datetime.datetime.today().strftime('%Y-%m-%d'),index=None)